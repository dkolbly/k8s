module bitbucket.org/dkolbly/k8s

go 1.14

require (
	bitbucket.org/dkolbly/logging v0.8.3
	k8s.io/api v0.18.0
	k8s.io/apimachinery v0.18.0
	k8s.io/client-go v0.18.0
)
