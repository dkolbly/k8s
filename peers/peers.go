// provides a handy way of finding our peers in a kubernetes world
//
// inspired by mailgun/gubernator

package peers

import (
	"context"
	"fmt"
	"io"

	"bitbucket.org/dkolbly/logging"
	api_v1 "k8s.io/api/core/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
)

var log = logging.New("peers")

type Pool struct {
	cfg  Config
	info cache.SharedIndexInformer
	sink DidUpdater
}

type Config struct {
	Namespace     string
	LabelSelector string
}

type UpdateReason int

const (
	UpdateInitial = UpdateReason(iota)
	UpdateAdd
	UpdateChange
	UpdateDelete
)

type DidUpdater interface {
	DidUpdate(UpdateReason, []Member)
}

type Member struct {
	IP    string
	Name  string
	Ports map[string]int
}

func New(ctx context.Context, config Config, sink DidUpdater) (*Pool, error) {
	p := &Pool{
		cfg:  config,
		sink: sink,
	}

	cc, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("K8S-1500 from InClusterConfig: %w", err)
	}

	// creates the client
	client, err := kubernetes.NewForConfig(cc)
	if err != nil {
		return nil, fmt.Errorf("K8S-1501 from NewForConfig: %w", err)
	}

	p.startup(ctx, client)
	return p, nil
}

func (p *Pool) Report(ctx context.Context, dst io.Writer) {

	if p.info == nil {
		fmt.Fprintf(dst, "not initialized\n")
		return
	}

	for _, obj := range p.info.GetStore().List() {
		endpoint, ok := obj.(*api_v1.Endpoints)
		if !ok {
			log.Errorf(ctx, "expected type v1.Endpoints but got %T", obj)
			continue
		}
		for i, s := range endpoint.Subsets {
			for j, addr := range s.Addresses {
				fmt.Fprintf(dst, "subset[%d] addr[%d] ip=%s (%s) ports:", i, j, addr.IP, addr.Hostname)
				for i, port := range s.Ports {
					if i > 0 {
						fmt.Fprintf(dst, ", ")
					}
					fmt.Fprintf(dst, " %d/%s", port.Port, port.Protocol)
					if port.Name != "" {
						fmt.Fprintf(dst, " (%s)", port.Name)
					} else {
						fmt.Fprintf(dst, "?")
					}
				}
				fmt.Fprintf(dst, "\n")
			}
		}
	}
}

func (p *Pool) Current() []Member {
	if p.info == nil {
		return nil
	}

	var lst []Member

	for _, obj := range p.info.GetStore().List() {
		endpoint, ok := obj.(*api_v1.Endpoints)
		if !ok {
			continue
		}
		for _, s := range endpoint.Subsets {
			for _, addr := range s.Addresses {
				m := Member{
					IP:    addr.IP,
					Name:  addr.Hostname,
					Ports: make(map[string]int),
				}
				for _, port := range s.Ports {
					if port.Name != "" {
						m.Ports[port.Name] = int(port.Port)
					}
				}
				lst = append(lst, m)
			}
		}
	}
	return lst
}

func (p *Pool) startup(ctx context.Context, c *kubernetes.Clientset) {
	log.Debugf(ctx, "K8S-1510 pool started, label selector is: %s", p.cfg.LabelSelector)

	informer := cache.NewSharedIndexInformer(
		&cache.ListWatch{
			ListFunc: func(options meta_v1.ListOptions) (runtime.Object, error) {
				options.LabelSelector = p.cfg.LabelSelector
				return c.CoreV1().Endpoints(p.cfg.Namespace).List(ctx, options)
			},
			WatchFunc: func(options meta_v1.ListOptions) (watch.Interface, error) {
				options.LabelSelector = p.cfg.LabelSelector
				return c.CoreV1().Endpoints(p.cfg.Namespace).Watch(ctx, options)
			},
		},
		&api_v1.Endpoints{},
		0, //Skip resync
		cache.Indexers{},
	)
	p.info = informer

	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err != nil {
				log.Errorf(ctx, "while calling MetaNamespaceKeyFunc(): %s", err)
				return
			}
			log.Debugf(ctx, "adding %s %T", key, obj)
			if p.sink != nil {
				p.sink.DidUpdate(UpdateAdd, p.Current())
			}
		},
		UpdateFunc: func(obj, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err != nil {
				log.Errorf(ctx, "while calling MetaNamespaceKeyFunc(): %s", err)
				return
			}
			log.Debugf(ctx, "updating %s %T", key, obj)
			if p.sink != nil {
				p.sink.DidUpdate(UpdateChange, p.Current())
			}
		},
		DeleteFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err != nil {
				log.Errorf(ctx, "while calling MetaNamespaceKeyFunc(): %s", err)
				return
			}
			log.Debugf(ctx, "deleting %s %T", key, obj)
			if p.sink != nil {
				p.sink.DidUpdate(UpdateDelete, p.Current())
			}
		},
	})

	done := make(chan struct{})

	go informer.Run(done)

	if !cache.WaitForCacheSync(done, informer.HasSynced) {
		close(done)
		log.Errorf(ctx, "timed out waiting for caches to sync")
	}
	if p.sink != nil {
		p.sink.DidUpdate(UpdateInitial, p.Current())
	}

	log.Debugf(ctx, "done bootstrapping")
}
