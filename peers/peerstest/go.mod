module bitbucket.org/dkolbly/k8s/peers/peerstest

go 1.14

require (
	bitbucket.org/dkolbly/k8s v0.0.0-20200527051755-fdf16c27b90e
	bitbucket.org/dkolbly/logging v0.8.3
	k8s.io/api v0.18.0
	k8s.io/apimachinery v0.18.0
	k8s.io/client-go v0.18.0
)
