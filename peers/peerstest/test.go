package main

import (
	"bufio"
	"context"
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/dkolbly/k8s/peers"
	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("omen4cam")

func main() {
	ctx := context.Background()

	mux := http.NewServeMux()
	mux.HandleFunc("/", setupPeersWatcher(ctx))
	http.ListenAndServe(":9040", mux)
}

func setupPeersWatcher(ctx context.Context) http.HandlerFunc {
	sel := os.Getenv("K8S_PEERS_LABEL_SELECTOR")
	if sel == "" {
		return nil
	}

	cfg := peers.Config{
		Namespace:     "default",
		LabelSelector: sel,
	}

	pool, err := peers.New(ctx, cfg, sink{})
	if err != nil {
		panic(err)
	}
	return func(w http.ResponseWriter, r *http.Request) {
		dst := bufio.NewWriter(w)
		defer dst.Flush()

		fmt.Fprintf(dst, "hello\n")
		for _, k := range os.Environ() {
			fmt.Fprintf(dst, "%s\n", k)
		}
		fmt.Fprintf(dst, "---------------- peer report --------------\n")
		pool.Report(r.Context(), dst)
	}
}

type sink struct {
}

func (_ sink) DidUpdate(why peers.UpdateReason, lst []peers.Member) {
	ctx := context.Background()
	log.Debugf(ctx, "update reason %d, now: %#v", why, lst)
}
