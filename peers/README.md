# Peers

This package helps keep track of peers in a Kubernetes group.  It's original use cases are twofold:

1. to support a deployment making use of `github.com/golang/groupcache`
2. to support a deployment doing sharding _a la_ cassandra

The intention is to support both StatefulSet deployments (for the latter case)
and replica sets (for the former case).

One of the tricky bits is figuring out how to make it easy to
configure a dev environment that lets you exercise as much of the
functionality as possible.

## Usage

Here is a simple program which serves up some information about the environment
and the state of the peer group in a report on the `/` url.


```
package main

import (
	"bufio"
	"context"
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/dkolbly/k8s/peers"
	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("omen4cam")

func main() {
	ctx := context.Background()

	mux := http.NewServeMux()
	mux.HandleFunc("/", setupPeersWatcher(ctx))
	http.ListenAndServe(":9040", mux)
}

func setupPeersWatcher(ctx context.Context) http.HandlerFunc {
	sel := os.Getenv("K8S_PEERS_LABEL_SELECTOR")
	if sel == "" {
		return nil
	}

	cfg := peers.Config{
		Namespace:     "default",
		LabelSelector: sel,
	}

	pool, err := peers.New(ctx, cfg, sink{})
	if err != nil {
		panic(err)
	}
	return func(w http.ResponseWriter, r *http.Request) {
		dst := bufio.NewWriter(w)
		defer dst.Flush()

		fmt.Fprintf(dst, "hello\n")
		for _, k := range os.Environ() {
			fmt.Fprintf(dst, "%s\n", k)
		}
		fmt.Fprintf(dst, "---------------- peer report --------------\n")
		pool.Report(r.Context(), dst)
	}
}

type sink struct {
}

func (_ sink) DidUpdate(why peers.UpdateReason, lst []peers.Member) {
	ctx := context.Background()
	log.Debugf(ctx, "update reason %d, now: %#v", why, lst)
}
```

### Deploying the Example

Here is how to deploy this example on microk8s using the local registry

```
cd peerstest
docker build -t localhost:32000/donovan/peerstest:v1 .
docker push localhost:32000/donovan/peerstest:v1
kubectl apply -f 01-service.yml
kubectl apply -f 02-deployment.yml
```

## Observations

In a replica set (as in the above example), the instances have their
own IPs but not their own names.  The names come out blank.

In a stateful set, the instances also get their own (stateful) names.

It appears that the order in the list is independent of which node you ask;
I suspect that's because they are all watching some notion of shared state
like in etcd or something.

If you don't specify `clusterIP: None` in the service.yml, then the
service as a whole gets an IP which then gets load-balanced across the
instances.
